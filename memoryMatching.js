
const shuffle = (array) => {
	let currentIndex = array.length,
		temporaryValue,
		randomIndex;
	
	while (0 !== currentIndex) {
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}
	return array;
}

function timeConverter(ms) {
	const minutes = Math.floor(ms / 60000);
	const seconds = ((ms % 60000) / 1000).toFixed(0);

	let minuteStr = minutes.toString(10);
    if (minuteStr.length === 1) {
		return `0${minutes}:${(seconds < 10 ? '0' : '') + seconds}`;
	} else {
		return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
	}
	
}

const onCardClick = (cardNumber) => {
	if (completedPairs.indexOf(cardNumber) === -1) { 
		cards[cardNumber].classList.toggle("flip"); 
		if (timerStart) { 
			timerStart = false;
			timer = setInterval(() => {
				ms += 1000;
				document.querySelector('.timer').innerHTML = timeConverter(ms);
			}, 1000);
		}
		if (cardPairs.indexOf(cardNumber) === -1) { 
			cardPairs.push(cardNumber);
			console.log('Cards Selected: ', cardPairs);
			if (cardPairs.length === 2) { 
				moveCounter += 1;
				document.querySelector('#moves').innerHTML = moveCounter;
				checkRating();
				if (cardNumbers[cardPairs[0]] === cardNumbers[cardPairs[1]]) { 
					completedPairs = completedPairs.concat(cardPairs);
					console.log('Completed Pairs: ', completedPairs);
					checkCompletion();
					cards[cardPairs[0]].querySelector('.back').style.background = '#13E2BA';
					cards[cardPairs[1]].querySelector('.back').style.background = '#13E2BA';
				} else { 
					let cardPairOne = cardPairs[0];
					let cardPairTwo = cardPairs[1];
					cards[cardPairs[0]].querySelector('.back').style.background = '#F95B3C';
					cards[cardPairs[1]].querySelector('.back').style.background = '#F95B3C';
					setTimeout(() => { 
						cards[cardPairOne].classList.toggle("flip");
						cards[cardPairTwo].classList.toggle("flip");
					}, 500);
					setTimeout(() => {
						cards[cardPairOne].querySelector('.back').style.background = '#12C1DF';
						cards[cardPairOne].querySelector('.back').style.background = '#12C1DF';
					}, 800);
				}
				cardPairs = []; 
			}
		} else {
			cardPairs = []; 
		}
	}
}

const checkRating = () => {
	if (moveCounter > 15) {
		rating = '&#9733&#9734&#9734';
		document.querySelector('.completed').innerHTML = `${rating} <span id="moves">${moveCounter}</span> Moves`;
	} else if (moveCounter > 10) {
		rating = '&#9733&#9733&#9734☆';
		document.querySelector('.completed').innerHTML = `${rating} <span id="moves">${moveCounter}</span> Moves`;
	}
}

const checkCompletion = () => {
	const completed = completedPairs.length;
	if (completed === 16) {
		clearInterval(timer);
		setTimeout(() => {
			document.querySelector('#winning-dialog').innerHTML = `You won in ${moveCounter} moves and ${timeConverter(ms)} minutes with a rating of ${rating} stars!`
			document.querySelector('.deck').style.display = 'none';
			document.querySelector('.counter-container').style.display = 'none';
			document.querySelector('.completed-container').style.display = 'flex';
		}, 1500);
	}
}
const resetGame = () => { 
	cardPairs = [];
	completedPairs = [];
	moveCounter = 0;
	clearInterval(timer);
	ms = 0;
	document.querySelector('.timer').innerHTML = timeConverter(ms);
	timerStart = true;
	rating = '&#9733&#9733&#9733';
	document.querySelector('.completed').innerHTML = `${rating} <span id="moves">${moveCounter}</span> Moves`;
	document.querySelector('#moves').innerHTML = 0;
	cardNumbers = shuffle(icons.concat(icons));
	for (let i = 0; i < cards.length; i++) {
		cards[i].classList.remove("flip");
	}
	setTimeout(() => { 
		for (let i = 0; i < cards.length; i++) {
			cards[i].querySelector('.back').style.background = '#12C1DF';
			cards[i].querySelector('.back').innerHTML = cardNumbers[i];
		}
	}, 500);
	document.querySelector('.deck').style.display = 'flex';
	document.querySelector('.counter-container').style.display = 'flex';
	document.querySelector('.completed-container').style.display = 'none';
	console.log('Game Reset');
}

let cards = document.querySelectorAll('.flip-deck'); 
let cardPairs = []; 
let completedPairs = []; 
let moveCounter = 0; 
let rating = '&#9733&#9733&#9733'; 
let ms = 0; 
let timerStart = true; 
let timer;
const icons = ['A', 'E', 'I', 'O', 'U', 'X', 'Y', 'Z']; 
let cardNumbers = shuffle(icons.concat(icons)); 
const reset = document.querySelector('.reset').addEventListener('click', () => resetGame());
const resetButton = document.querySelector('.reset-button').addEventListener('click', () => resetGame());
for (let i = 0; i < cards.length; i++) {
	cards[i].querySelector('.back').innerHTML = cardNumbers[i]; 
	cards[i].addEventListener('click', (e) => onCardClick(i));
}
